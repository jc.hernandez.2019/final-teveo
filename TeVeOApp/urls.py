from django.urls import path
from . import views

urlpatterns = [
    path('', views.pagina_principal, name='pagina_principal'),
    path('configuracion/', views.pagina_configuracion, name='pagina_configuracion'),
    path('ayuda/', views.pagina_ayuda, name='pagina_ayuda'),
    path('cerrar_sesion/', views.cerrar_sesion, name='cerrar_sesion'),
    path('camaras/', views.pagina_camaras, name='pagina_camaras'),
    path('camara/<str:id_camara>/', views.pagina_camara, name='pagina_camara'),
    path('camara/json/<str:id_camara>/', views.pagina_camara_json, name='pagina_camara_json'),
    path('camara/<str:id_camara>/comentario/', views.pagina_comentario, name='pagina_comentario'),
    path('camara/dyn/<str:id_camara>/', views.pagina_camara_dinamica, name='pagina_camara_dinamica'),
    path('camara/dyn/<str:id_camara>/image', views.descargar_y_devolver_imagen, name='descargar_y_devolver_imagen'),

]
