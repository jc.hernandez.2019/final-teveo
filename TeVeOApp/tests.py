from django.test import TestCase, Client
from django.urls import reverse
from .models import Camara, Comentario, ConfiguracionUsuario
from django.contrib.auth.models import User
from django.utils import timezone

class CamaraTests(TestCase):

    def setUp(self):
        # Crea un usuario de prueba
        self.config_user = ConfiguracionUsuario.objects.create(nombre_usuario='Test User')

        # Crea cámaras de prueba
        self.camara1 = Camara.objects.create(
            id_camara='1',
            nombre='Camara Test 1',
            coordenadas='40.416775,-3.703790',
            src='https://example.com/image1.jpg'
        )

        self.camara2 = Camara.objects.create(
            id_camara='A',
            nombre='Camara Test A',
            coordenadas='41.403629,2.174356',
            src='https://example.com/imageA.jpg'
        )

        # Crea comentarios de prueba
        self.comentario1 = Comentario.objects.create(
            camara=self.camara1,
            usuario=self.config_user,
            texto='Este es un comentario de prueba',
            fecha=timezone.now(),
            imagen_url='ruta/a/imagen.jpg'
        )

    def test_pagina_camara_dinamica_view(self):
        client = Client()
        response = client.get(reverse('pagina_camara_dinamica', args=[self.camara1.id_camara]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.camara1.nombre)
        self.assertContains(response, self.camara1.coordenadas)
        self.assertContains(response, self.comentario1.texto)



    def test_invalid_camara_id(self):
        client = Client()
        response = client.get(reverse('pagina_camara_dinamica', args=['invalid']))
        self.assertEqual(response.status_code, 404)



class ComentarioTests(TestCase):

    def setUp(self):
        # Crea un usuario de prueba
        self.config_user = ConfiguracionUsuario.objects.create(nombre_usuario='Test User')

        # Crea cámaras de prueba
        self.camara1 = Camara.objects.create(
            id_camara='1',
            nombre='Camara Test 1',
            coordenadas='40.416775,-3.703790',
            src='https://example.com/image1.jpg'
        )

        # Crea comentarios de prueba
        self.comentario1 = Comentario.objects.create(
            camara=self.camara1,
            usuario=self.config_user,
            texto='Este es un comentario de prueba',
            fecha=timezone.now(),
            imagen_url='ruta/a/imagen.jpg'
        )

    def test_comentario_content(self):
        comentario = Comentario.objects.get(id=self.comentario1.id)
        expected_object_name = f'{comentario.texto}'
        self.assertEqual(expected_object_name, 'Este es un comentario de prueba')

    def test_comentario_fecha(self):
        comentario = Comentario.objects.get(id=self.comentario1.id)
        self.assertEqual(comentario.fecha.date(), timezone.now().date())

class ConfiguracionUsuarioTests(TestCase):

    def setUp(self):
        self.config_user = ConfiguracionUsuario.objects.create(nombre_usuario='Test User')

    def test_usuario_nombre(self):
        config_user = ConfiguracionUsuario.objects.get(id=self.config_user.id)
        expected_object_name = f'{config_user.nombre_usuario}'
        self.assertEqual(expected_object_name, 'Test User')
