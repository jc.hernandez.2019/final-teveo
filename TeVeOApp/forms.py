from django import forms
from .models import ConfiguracionUsuario, Comentario


class UserSetupForm(forms.ModelForm):
    TIPO_FUENTE_CHOICES = [
        ('Arial', 'Arial'),
        ('Verdana', 'Verdana'),
        ('Times New Roman', 'Times New Roman')
    ]

    TAMANO_FUENTE_CHOICES = [
        ('x-small', 'Pequeño'),
        ('medium', 'Mediano'),
        ('x-large', 'Grande')
    ]

    tipo_fuente = forms.ChoiceField(choices=TIPO_FUENTE_CHOICES, label="Tipo de Fuente")
    tamano_fuente = forms.ChoiceField(choices=TAMANO_FUENTE_CHOICES, label="Tamaño de Fuente")

    class Meta:
        model = ConfiguracionUsuario
        fields = ['nombre_usuario', 'tipo_fuente', 'tamano_fuente']


class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']
