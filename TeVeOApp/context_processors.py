from .models import Camara, Comentario


def contadores(request):
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    nombre_usuario = request.session.get('nombre_usuario', 'Anónimo')
    tamano_fuente = request.session.get('tamano_fuente', '16px')
    tipo_fuente = request.session.get('tipo_fuente', 'Arial')
    return {
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,
        'nombre_usuario': nombre_usuario,
        'tamano_fuente': tamano_fuente,
        'tipo_fuente': tipo_fuente,
    }