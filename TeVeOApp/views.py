import base64
import os.path
import os

from django.contrib.sites import requests
from django.core.paginator import Paginator
from django.conf import settings
from django.utils import timezone
from .models import Comentario, Camara, ConfiguracionUsuario
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from datetime import datetime
import xml.etree.ElementTree as ET
import urllib.request
from .forms import UserSetupForm, ComentarioForm
import random
from django.db.models import Count
import requests
import pytz




# -------------------------------------------------------------------------------------------------------------

def cerrar_sesion(request):
    request.session.flush()
    return redirect('pagina_principal')


# -------------------------------------------------------------------------------------------------------------

def pagina_ayuda(request):
    return render(request, 'pagina_ayuda.html')


# -------------------------------------------------------------------------------------------------------------

def pagina_configuracion(request):
    if request.method == 'POST':
        form = UserSetupForm(request.POST)
        if form.is_valid():
            user_setup = form.save(commit=False)
            request.session['nombre_usuario'] = user_setup.nombre_usuario
            request.session['tamano_fuente'] = user_setup.tamano_fuente
            request.session['tipo_fuente'] = user_setup.tipo_fuente

            # Crear o actualizar ConfiguracionUsuario
            ConfiguracionUsuario.objects.update_or_create(
                nombre_usuario=user_setup.nombre_usuario,
                defaults={
                    'tamano_fuente': user_setup.tamano_fuente,
                    'tipo_fuente': user_setup.tipo_fuente
                }
            )

            return redirect('pagina_principal')
    else:
        initial_data = {
            'nombre_usuario': request.session.get('nombre_usuario', 'Anónimo'),
            'tamano_fuente': request.session.get('tamano_fuente', 'Mediana'),
            'tipo_fuente': request.session.get('tipo_fuente', 'Arial'),
        }
        form = UserSetupForm(initial=initial_data)

    return render(request, 'pagina_configuracion.html', {'form': form})


# -------------------------------------------------------------------------------------------------------------#
def pagina_principal(request):
    comentarios_list = Comentario.objects.all().order_by('-fecha')
    paginator = Paginator(comentarios_list, 5)  # 5 comentarios por página

    page_number = request.GET.get('page')
    comentario = paginator.get_page(page_number)

    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()

    context = {
        'comentario': comentario,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios
    }

    return render(request, 'pagina_principal.html', context)


# -------------------------------------------------------------------------------------------------------------
def descargar_bases_datos(base_datos):
    if base_datos == 'listado1.xml':
        url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml'
    elif base_datos == 'listado2.xml':
        url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml'
    elif base_datos == 'listadoextra.xml':
        url = 'https://infocar.dgt.es/datex2/dgt/CCTVSiteTablePublication/all/content.xml'
    elif base_datos == 'listadoextra2.xml':
        url = 'https://opendata.euskadi.eus/contenidos/ds_localizaciones/camaras_trafico/opendata/camaras-trafico.xml'

    # Descarga el archivo XML
    response = urllib.request.urlopen(url)
    data = response.read().decode('utf-8')
    # Procesa el XML y almacena la información en la base de datos
    root = ET.fromstring(data)

    for camara in root.findall('camara'):
        if base_datos == 'listado1.xml':
            id = camara.find('id').text
            id = 'LIS1-' + id
            src = camara.find('src').text
            lugar = camara.find('lugar').text
            coordenadas = camara.find('coordenadas').text
            # Verifica si la cámara ya existe en la base de datos antes de guardarla
            if not Camara.objects.filter(id_camara=id).exists():
                Camara.objects.create(id_camara=id,
                                      src=src,
                                      nombre=lugar,
                                      coordenadas=coordenadas)
    for cam in root.findall('cam'):
        if base_datos == 'listado2.xml':
            id = cam.get('id')
            id = 'LIS2-' + id
            url = cam.find('url').text
            info = cam.find('info').text
            latitude = cam.find('place/latitude').text
            longitude = cam.find('place/longitude').text
            coordenadas = latitude + ',' + longitude

            if not Camara.objects.filter(id_camara=id).exists():
                Camara.objects.create(id_camara=id,
                                      src=url,
                                      nombre=info,
                                      coordenadas=coordenadas)

    if root.tag.endswith('d2LogicalModel'):
        i = 0
        for cam in root.findall('.//_0:cctvCameraMetadataRecord',
                              namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}):
                i += 1
                id = "LIS3-" + str(i)
                info = cam.find('_0:cctvCameraIdentification',
                                           namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                url = cam.find('.//_0:urlLinkAddress',
                                          namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_latitude = cam.find('.//_0:latitude',
                                               namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_longitude = cam.find('.//_0:longitude',
                                                namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                coordenadas = "{},{}".format(cam_latitude, cam_longitude)


                if not Camara.objects.filter(id_camara=id).exists():
                    Camara.objects.create(id_camara=id,
                                          src=url,
                                          nombre=info,
                                          coordenadas=coordenadas)

    for cam in root.findall('row'):
        if base_datos == 'listadoextra2.xml':
            id = cam.get('num')
            id = 'LIS4-' + id
            url = cam.find('urlcam').text
            info = cam.find('title').text
            latitude = cam.find('latwgs84').text
            longitude = cam.find('lonwgs84').text
            coordenadas = latitude + ',' + longitude

            if not Camara.objects.filter(id_camara=id).exists():
                Camara.objects.create(id_camara=id,
                                      src=url,
                                      nombre=info,
                                      coordenadas=coordenadas)





# -------------------------------------------------------------------------------------------------------------

def pagina_camaras(request):
    bases = {
        'listado1.xml': 'Listado 1',
        'listado2.xml': 'Listado 2',
        'listadoextra.xml': 'Listado extra(DGT)',
        'listadoextra2.xml': 'Listado extra 2(Euskadi)'

    }

    if request.method == 'POST':
        base_datos = request.POST.get('base_datos')
        descargar_bases_datos(base_datos)

    camaras = Camara.objects.all().annotate(total_comentarios=Count('comentario')).order_by('-total_comentarios')
    # Configurar el paginador para mostrar 4 camaras por página
    paginator = Paginator(camaras, 4)
    page_number = request.GET.get('page')
    cameras = paginator.get_page(page_number)
    if cameras:
        camara_aleatoria = random.choice(cameras)
    else:
        camara_aleatoria = None

    context = {
        'bases': bases,
        'cameras': cameras,
        'camara_aleatoria': camara_aleatoria,
    }

    return render(request, 'pagina_camaras.html', context)


# -------------------------------------------------------------------------------------------------------------


def pagina_camara(request, id_camara):
    camara = get_object_or_404(Camara, id_camara=id_camara)
    comentarios_list = Comentario.objects.filter(camara=camara).order_by('-fecha')

    # Configurar el paginador para mostrar 5 comentarios por página
    paginator = Paginator(comentarios_list, 5)
    page_number = request.GET.get('page')
    comentarios = paginator.get_page(page_number)

    context = {
        'camara': camara,
        'comentarios': comentarios,
    }
    return render(request, 'pagina_camara.html', context)


# -------------------------------------------------------------------------------------------------------------

def descargar_y_devolver_imagen(request, id_camara):
    camara = get_object_or_404(Camara, id_camara=id_camara)
    url_imagen = camara.src
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }

    try:
        response = requests.get(url_imagen, headers=headers)
        response.raise_for_status()
        image_base64 = base64.b64encode(response.content).decode('utf-8')
        html = f'<img src="data:image/jpeg;base64,{image_base64}" class="card-img-top" alt="Camera Image" style="width:100%; height:auto;">'
        return HttpResponse(html, content_type="text/html")
    except requests.RequestException as e:
        return HttpResponse(str(e), status=500)


def pagina_camara_dinamica(request, id_camara):
    camara = get_object_or_404(Camara, id_camara=id_camara)
    comentarios = Comentario.objects.filter(camara=camara).order_by('-fecha')

    context = {
        'camara': camara,
        'comentarios': comentarios,
    }

    return render(request, 'pagina_camara_dinamica.html', context)


# -------------------------------------------------------------------------------------------------------------


def pagina_camara_json(request, id_camara):
    camara = Camara.objects.get(id_camara=id_camara)
    data = []
    data.append({
        'id': camara.id_camara,
        'nombre': camara.nombre,
        'ubicacion': camara.coordenadas,
        'fuente': camara.src
    })
    return JsonResponse(data, safe=False)


# -------------------------------------------------------------------------------------------------------------


def descargar_imagen(url, filename):
    file_path = os.path.join(settings.BASE_DIR, 'TeVeOApp/static/imagen_comentarios', filename)
    urllib.request.urlretrieve(url, file_path)


def pagina_comentario(request, id_camara):
    camara = get_object_or_404(Camara, id_camara=id_camara)
    nombre_usuario = request.session.get('nombre_usuario', 'Anónimo')

    usuario, created = ConfiguracionUsuario.objects.get_or_create(
        nombre_usuario=nombre_usuario,
        defaults={
            'tamano_fuente': request.session.get('tamano_fuente', 'medium'),
            'tipo_fuente': request.session.get('tipo_fuente', 'Arial'),
        }
    )

    if request.method == 'POST':
        form = ComentarioForm(request.POST)
        if form.is_valid():
            texto_comentario = form.cleaned_data['texto']
            imagen_url = camara.src

            # Generar un nombre de archivo basado en la fecha y hora actual
            timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
            imagen_filename = f"{timestamp}.jpg"

            # Descargar la imagen con el nuevo nombre de archivo

            descargar_imagen(imagen_url, imagen_filename)
            zona_horaria_espana = pytz.timezone('Europe/Madrid')
            # Crear el comentario con la imagen descargada
            comentario = Comentario(
                camara=camara,
                usuario=usuario,  # Asigna la instancia del usuario configurado
                texto=texto_comentario,
                fecha=timezone.now(),
                imagen_url=imagen_filename  # Guardar el nombre del archivo de la imagen
            )
            comentario.save()
            return redirect('pagina_camara', id_camara=id_camara)
    else:
        form = ComentarioForm()

    fecha_actual = timezone.now()
    context = {
        'camara': camara,
        'form': form,
        'fecha_actual': fecha_actual,
    }
    return render(request, 'pagina_comentario.html', context)

# -------------------------------------------------------------------------------------------------------------
