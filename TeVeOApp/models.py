from django.db import models
from django.conf import settings


class ConfiguracionUsuario(models.Model):
    nombre_usuario = models.CharField(max_length=100, default='Anónimo')
    tamano_fuente = models.CharField(max_length=20, default='medium')
    tipo_fuente = models.CharField(max_length=50, default='Arial')

    def __str__(self):
        return self.nombre_usuario, self.tamano_fuente, self.tipo_fuente


class Camara(models.Model):
    id_camara = models.CharField(max_length=50)
    nombre = models.CharField(max_length=100)
    coordenadas = models.CharField(max_length=100)  # Puede ser una combinación de longitud y latitud.(listado2.xml)
    src = models.URLField()  # URL de la imagen o del stream de la cámara.

    def __str__(self):
        return self.nombre


class Comentario(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    usuario = models.ForeignKey(ConfiguracionUsuario, on_delete=models.CASCADE)
    texto = models.TextField()
    fecha = models.DateTimeField()
    imagen_url = models.FilePathField(path=f'{settings.BASE_DIR}/imagen_comentarios/', blank=False)

    def __str__(self):
        return f"Comentario por {self.camara} en {self.fecha}"