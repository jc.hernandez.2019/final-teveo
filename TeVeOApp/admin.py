from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Camara, Comentario, ConfiguracionUsuario

admin.site.register(Camara)
admin.site.register(Comentario)
admin.site.register(ConfiguracionUsuario)
