# Final-TeVeO

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jose Carlos Hernandez Gajardo
* Titulación: Ingenieria en Sistemas de Telecomunicacion
* Cuenta en laboratorios: josehdz
* Cuenta URJC: jc.hernandez.2019@alumnos.urjc.es
* Video básico (url):https://www.youtube.com/watch?v=iWWcq4iiCI4
* Video parte opcional (url):https://www.youtube.com/watch?v=cbIn5kXR3tc 
* Despliegue (url): https://josehdzz.pythonanywhere.com/
* Contraseñas: **no hay ninguna conytraseña necesaria
* Cuenta Admin Site: admin/contraseña

## Resumen parte obligatoria
    La aplicacion web TeVeO se encuentra dividido en las siguientes partes obligatorias:
    
    Pagina principal: En esta pagin encontramos los comentarior ordenados de mas recientes,
    y los cuales contienen la fecha, el autor y la imagen en el momento del comentario.
    
    Pagina de las camaras: Encontramos 4 listados de camaras disponibles, la imagen de 
    una camara aleatoria y por ultimo un listado con todas las camaras ordenadas de mas 
    a menos comnetarios.

    Pagina de la camara estatica: En esta pagina se muestra la imagen de la camara al   
    momento de acceder a la pagina, un listado con los comentarios de esa camara ordenados 
    por mas recientes y por ultimo los enlaces a la camara dinaica, camara json y pagina de 
    comentarios de esa camara.

    Pagina de la camara dinamica: Es igual que la anterior pero la imagen de la camara se 
    actualiza cada 30 segundos.

    Pagina de los comentarios: Aqui encontramos la imagen de la camara junto a la fecha actual
    y un formulario para rellenar con el texto a comentar

    Pagina json: pagina en formato json, con los datos de la camara.

    Pagina de ayuda: Breve explicacon de la aplicacion:

    Pagina de admin: Admin de Django

    
## Lista partes opcionales

*  Nombre parte: Paginado de los comentarios tanto en la pagina principal como en cada una
de las camaras y tambien paginado en el listado de las camaras que tenemos descargadas de
las bases de datos
* Nombre parte: Fav icon, se ha añadido el icono de la aplicacion
* Nombre parte: Cerrar sesion, se puede cerrar la sesion del usuario
* Nombre parte: Bases de datos extra, se han añadido dos bases de datos que permiten tener 
una gran cantidad de camaras disponibles.

